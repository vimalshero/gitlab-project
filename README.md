in this project we use nodejs Application.
architecture i used in this project (aws eks cluster,docker,git-lab,git-lab container registry)

**how to install eks cluster**
- first provision server with t2.medium configuration
- install docker
- install aws cli
- install kubectl
- install eksctl
- eksctl create cluster --name=eksdemo1 
- eksctl create nodegroup --cluster=eksdemo1

**install git-lab runner**
- for ubuntu `curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
- sudo apt-get install gitlab-runner
- sudo git-lab runner status

**git-lab**
- in git-lab /settings/cicd/runners in that create a project runner
- project runner/tags(eks)
- copy and paste gitlab-runner register in your k8s cluster.
- $ git-lab runner register
    -- url ()
    -- token()

**Installing the agent for Kubernetes**[](https://docs.gitlab.com/ee/user/clusters/agent/install/#create-an-agent-configuration-file)
~~Create an agent configuration file~~  
-.gitlab/agents/<agent-name>/config.yaml in which agent-name is our k8s cluster name(eksdemo1).

** GitOps section of an agent configuration file (config.yaml).**[](https://docs.gitlab.com/16.9/ee/user/clusters/agent/gitops/agent.html)
- in which change -projectid ,branch,namespace,path = global file

**register gitlab agent with kubernetes cluster**
go to operates/kunernetes cluster / connect kubernets cluster
- install by using helm command
`helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install test gitlab/gitlab-agent \
    --namespace gitlab-agent-test \
    --create-namespace \
    --set image.tag=<current agentk version> \
    --set config.token=<your_token> \
    --set config.kasAddress=<address_to_GitLab_KAS_instance>
**add image name**
- go to the container registry then copy the image name
  containers:
        - name: devops
          image: registry.gitlab.com/vimalshero/gitlab-project:latest
**

**add gitlab container registry secrets in eks deployment yaml file**[](https://docs.gitlab.com/16.9/ee/user/clusters/agent/gitops/secrets_management.html)`

in gitlab go to the settings/repository/deploy tokens create new token it will give <gitlab-username> and <gitlab-token>

kubectl create secret docker-registry gitlab-credentials --docker-server=registry.gitlab.example.com --docker-username=<gitlab-username> --docker-password=<gitlab-token> --dry-run=client -o yaml > gitlab-credentials.yaml

- kubectl apply -f gitlab-credentials.yaml

-add the secret credentials into the node-app.yaml file 
` spec:
      imagePullSecrets:
        - name: registry-credentials
`
**Create a .gitlab-ci.yml file**[](https://docs.gitlab.com/ee/ci/)
varaiables:
 KUBE_CONTEXT: path/to/agent/project:agent-name

- add 
    stages:
    - docker build
    - deploy to EKS

**add varaiables for the pipeline**
 go to settings/cicd/variables then add varaiables $CI_REGISTRY_USER,$CI_REGISTRY_PASSWORD,$CI_REGISTRY

 **docker build stage**
 -build the image and push into gitlab container registry
 Build docker image:
    stage: docker build
         script:
        - docker build -t $CI_REGISTRY .
        - docker push $CI_REGISTRY


 **deploy to the eks cluster**[](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html)
 
 - deploy to the cluster on different namespace
 connect to AWS EKS:
    stage: deploy to EKS
    image:
        name: bitnami/kubectl:latest
        entrypoint: ['']
        script:
        - kubectl create namespace staging --dry-run=client -o yaml | kubectl apply -f -
        - kubectl apply -f node-app.yaml --namespace staging



 **now start the pipeline**


**description of roll back strategy and how it would be triggered and executed **
- rollback strategy involves reverting to a previous version of a deployment or configuration when a new deployment introduces issues or fails to meet expectations.

-by using rolling update,blue green deployment and canary deployment we can roll back changes gradually

-kubectl rollout undo deployment <deployment-name>




 







  `



